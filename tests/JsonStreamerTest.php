<?php

namespace Test;

use JsonStreamer\JsonStreamer;
use PHPUnit\Framework\TestCase;

class JsonStreamerTest extends TestCase
{
    public function testEmptyArray()
    {
        $fp = fopen('data://text/plain,[]','r');
        $parser = new JsonStreamer($fp, "");
        $values = iterator_to_array($parser);
        $this->assertEquals([], $values);
    }

    public function testArrayOfObjects()
    {
        $fp = fopen('data://text/plain,[{"a":"b"},{"c":"d"}]','r');
        $parser = new JsonStreamer($fp, "");
        $parser->setAssoc(true);
        $values = iterator_to_array($parser);
        $this->assertEquals([["a" => "b"], ["c" => "d"]], $values);
    }

    public function testArrayOfArrays()
    {
        $fp = fopen('data://text/plain,[["a","b"],["c","d"]]','r');
        $parser = new JsonStreamer($fp, "");
        $parser->setAssoc(true);
        $values = iterator_to_array($parser);
        $this->assertEquals([["a", "b"], ["c", "d"]], $values);
    }

    public function testNamedArrayOfObjects()
    {
        $fp = fopen('data://text/plain,{"name":[{"a":"b"},{"c":"d"}]}','r');
        $parser = new JsonStreamer($fp, "name");
        $parser->setAssoc(true);
        $values = iterator_to_array($parser);
        $this->assertEquals([["a" => "b"], ["c" => "d"]], $values);
    }

    public function testNamedArrayOfArrays()
    {
        $fp = fopen('data://text/plain,{"name":[["a","b"],["c","d"]]}','r');
        $parser = new JsonStreamer($fp, "name");
        $parser->setAssoc(true);
        $values = iterator_to_array($parser);
        $this->assertEquals([["a", "b"], ["c", "d"]], $values);
    }

    public function testNestedArrayOfObjects()
    {
        $fp = fopen('data://text/plain,{"lvl1":{"lvl2":{"lvl3":[{"a":"b"},{"c":"d"}]}}}','r');
        $parser = new JsonStreamer($fp, "lvl1.lvl2.lvl3");
        $parser->setAssoc(true);
        $values = iterator_to_array($parser);
        $this->assertEquals([["a" => "b"], ["c" => "d"]], $values);
    }

    public function testNestedArrayOfArrays()
    {
        $fp = fopen('data://text/plain,{"lvl1":{"lvl2":{"lvl3":[["a","b"],["c","d"]]}','r');
        $parser = new JsonStreamer($fp, "lvl1.lvl2.lvl3");
        $parser->setAssoc(true);
        $values = iterator_to_array($parser);
        $this->assertEquals([["a", "b"], ["c", "d"]], $values);
    }

    /**
     * @param $input
     * @dataProvider provideInvalidJSON
     */
    public function testInvalidJSON($input)
    {
        $this->expectException(\RuntimeException::class);
        $fp = fopen('data://text/plain,' . $input,'r');
        $parser = new JsonStreamer($fp, "");
        iterator_to_array($parser);
    }

    public function provideInvalidJSON()
    {
        return [
            "array of strings" => ['["s"]'],
            "array of numbers" => ['[1, 1.3]'],
            "missing comma" => ['[[] []]'],
            "extra comma" => ['[[],, []]'],
            "trailing comma" => ['[[], [], ]'],
        ];
    }
}