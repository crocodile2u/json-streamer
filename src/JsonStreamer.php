<?php

namespace JsonStreamer;

use Bcn\Component\Json\Reader\Tokenizer;

class JsonStreamer implements \IteratorAggregate {
    /**
     * @var resource
     */
    private $stream;
    /**
     * @var string
     */
    private $path;

    /**
     * @var bool
     */
    private $assoc = false;

    /**
     * Jsontream constructor.
     * @param resource $stream
     * @param string $path
     */
    public function __construct($stream, string $path)
    {
        $this->stream = $stream;
        $this->path = $path;
    }

    /**
     * @param bool $assoc
     */
    public function setAssoc(bool $assoc): void
    {
        $this->assoc = $assoc;
    }

    public function getIterator(): \Iterator
    {
        rewind($this->stream);
        return $this->parse();
    }

    private function parse(): \Generator
    {
        $tokenizer = new \Bcn\Component\Json\Reader\Tokenizer($this->stream);
        $path = "";
        $startChars = [
            Tokenizer::TOKEN_OBJECT_START => "{",
            Tokenizer::TOKEN_ARRAY_START => "[",
        ];
        $endChars = [
            Tokenizer::TOKEN_OBJECT_START => "}",
            Tokenizer::TOKEN_ARRAY_START => "]",
        ];
        while ($token = $tokenizer->next()) {
            $path = $this->path($path, $token);
            if ($path === $this->path) {
                if ($token = $tokenizer->next()) {
                    if (Tokenizer::TOKEN_ARRAY_END === $token["token"]) {
                        return;
                    }
                    if ($this->isStreamable($token["token"])) {

                        $startChar = $startChars[$token["token"]];// our buffer starts with it
                        $endChar = $endChars[$token["token"]];// our buffer will end with it

                        // our JSON that we're gonna return on every iteration
                        $buffer = $startChar;

                        // now we don't need tokenizer any more, we just read char by char
                        while (!feof($this->stream)) {
                            $char = fread($this->stream, 1);
                            $buffer .= $char;
                            if ($char === $endChar) {
                                // this portion of JSON might be valid now
                                $json = json_decode($buffer, $this->assoc);
                                if (null !== $json) {// YAY, got a valid JSON on this iteration

                                    yield $json;

                                    $buffer = "";// reset buffer before proceeding to the next chunk

                                    $expectNextIteration = false;
                                    while (!feof($this->stream)) {
                                        $char = fread($this->stream, 1);
                                        if ($char === ",") {
                                            if ($expectNextIteration) {
                                                throw new \RuntimeException(
                                                    "Invalid JSON: expecting next iteration, got comma"
                                                );
                                            }
                                            $expectNextIteration = true;
                                        } elseif ($char === $startChar) {
                                            if (!$expectNextIteration) {
                                                throw new \RuntimeException(
                                                    "Invalid JSON: no comma between iterations"
                                                );
                                            }
                                            $buffer = $startChar;
                                            break;
                                        } elseif ($char === "]") {
                                            if ($expectNextIteration) {
                                                throw new \RuntimeException(
                                                    "Invalid JSON: expecting next iteration, got array termination ( ] )"
                                                );
                                            }
                                            break 2;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        throw new \RuntimeException("Unexpected token: " . json_encode($token));
                    }
                }
                break;
            }
        }
    }

    private function isStreamable(int $token)
    {
        return (Tokenizer::TOKEN_OBJECT_START === $token) || (Tokenizer::TOKEN_ARRAY_START === $token);
    }

    private function path(string $base, array $token)
    {
        if ((Tokenizer::TOKEN_ARRAY_START === $token["token"]) || (Tokenizer::TOKEN_OBJECT_START === $token["token"])) {
            $p = ltrim("$base.{$token["key"]}", ".");
            if ($p === $this->path) {
                return $p;
            }
            if ($p === "") {
                return $base;
            }

            if (0 === strpos($this->path, $p)) {// we're on the right path
                return $p;
            }
        }

        return $base;
    }
}