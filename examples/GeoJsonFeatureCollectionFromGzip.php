<?php

include __DIR__ . "/../vendor/autoload.php";

$fp = fopen("compress.zlib://" . __DIR__ . "/Fiji.GeoJson.gz", "r");
$parser = new \JsonStreamer\JsonStreamer($fp, "features");
foreach ($parser as $val) {
    echo json_encode($val) . "\n\n";
}