<?php

include __DIR__ . "/../vendor/autoload.php";

$json = json_encode([
    "lvl1" => [
        "lvl2" => [
            "lvl3" => [
                [1, 2, 3],
                [1, 2, 3, 4],
                [1, 2, 3, 4, 5],
                [1, 2, 3, 4, 5, 6],
            ]
        ]
    ],
], JSON_PRETTY_PRINT);

$fp = fopen('data://text/plain,' . $json,'r');
$parser = new \JsonStreamer\JsonStreamer($fp, "lvl1.lvl2.lvl3");
foreach ($parser as $val) {
    echo json_encode($val) . "\n\n";
}